const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const usersModel = new Schema(
  {
    userNamePrefix: String ,
    userNameF:String,
    userNameL:String,
    userEmail:String,
    userPassword:String,
    profession:String,
    nameOfOrganization:String,
    country:String,

    countryCode:String,

    phone:String,

    addL1:String,
    addL2:String,
    city:String,
    postalCode:String,
    state:String,
    registrationFees:String,
    vAIMembershipNumber:String,
    registrationNumber:String,
    Attendee:String,
    userToken:String,

    









    
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("vicikonusers", usersModel);

// import * as mongoose from "mongoose";

// export interface usersModelTypes extends mongoose.Document {
//   name: String;
//   age: Number;
//   email: String;
//   status: Boolean;
//   phoneNumber: Number;
// }

// export const usersModelSchema = new mongoose.Schema(
//   {
//     name: { type: String, required: true },
//     IpVoted: { type: Array }
//   },
//   {
//     timestamps: true
//   }
// );

// const userModel = mongoose.model<usersModelTypes>("surveys", usersModelSchema);
// export default userModel;

const mongoose = require("mongoose");
// const SurveyTypes = require("../graphql/types/surveys");
const SurveySchema = new mongoose.Schema(
  {
    userName: { type: String },
    userEmail: { type: String, required: true },
    userId: { type: String },
   
    zoneView:{type:Array}
    
  },
  {
    timestamps: true,
  }
);

const userLogModel = mongoose.model("userloginfos", SurveySchema);
module.exports = userLogModel;
